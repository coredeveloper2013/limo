var forgetPassword = {
    getForgetPassword: function() {
        $('#userLoginDiv').on("submit", function(event) {
            event.preventDefault();

            var getLocalStorageValue = window.localStorage.getItem("limoanyWhereVerification");
            if (typeof(getLocalStorageValue) == 'string') {
                getLocalStorageValue = JSON.parse(getLocalStorageValue);
            }

            var fd = new FormData($('#userLoginDiv')[0]);
            fd.append("action", "forgetPassword");
            fd.append("user_id", getLocalStorageValue[0].user_id);
            $.ajax({
                url: _SERVICEPATHSERVICECLIENT,
                type: 'POST',
                processData: false,
                contentType: false,
                dataType: 'json',
                data: fd
            }).done(function(result) {

                console.log("result....", result);
                if(result.code==1006)
                {

                    alert(result.data.RetrieveInfo);
                    window.location.href="login.html";

                }
                else
                {

                    alert("wrong retrive type or Retrive value");
                }


            });
        });
    }
}
forgetPassword.getForgetPassword();