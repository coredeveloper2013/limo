
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Vehicle List
  Author: Mylimoproject
---------------------------------------------*/
/*set the path of php file for the calling web service*/
   var _SERVICEPATH="phpfile/service.php";
   var defaultImagePath='images/noImage.jpg';
 
/*call the getvehicle function on page load*/  
   //getVehicle();
 
 /*---------------------------------------------
    Function Name: getVehicle
    Input Parameter:limoApiID,limoApiKey,user_id
    return:list of vehicle
 ---------------------------------------------*/ 
    function getVehicle()
    {

		$('#refresh_overlay').css("display","block");
	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}
	    $('#userName').html(getuserId[0].full_name);
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
		if(typeof(getLocalStoragevalue)=="string")
		{
		   	getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
	    }

		$.ajax({
			url: _SERVICEPATH,
			type: 'POST',
			data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
			success: function(response) 
	        {	

	         		
	        	var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string")
				{
				   responseObj=JSON.parse(response);					
				}

	           if(responseObj.ResponseCode==0){
				var getFinal=[];
				for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) 
				{
					getFinal[i]=responseObj.VehicleTypes.VehicleType[i].PassengerCapacity+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'@'+responseObj.VehicleTypes.VehicleType[i].VehTypeId;
				}
					
				for(var j=0; j<getFinal.length; j++)
				{
					var carsplitResult=getFinal[j].split('@');
					for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) 
					{
						if(carsplitResult[1]==responseObj.VehicleTypes.VehicleType[i].VehTypeId)
						{
							if(typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1)!="undefined" && typeof(responseObj.VehicleTypes.VehicleType[i].VehTypeImg1)!=undefined)
							{
								responseHTML+='<tr id="vehicle_type_1994"> <td>'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td><td>'+responseObj.VehicleTypes.VehicleType[i].PassengerCapacity+'</td><td><strong>'+responseObj.VehicleTypes.VehicleType[i].VehTypeTitle+'</strong> <br><img src="'+responseObj.VehicleTypes.VehicleType[i].VehTypeImg1+'" width="174" height="120" class="img-polaroid"> </td><td>'+responseObj.VehicleTypes.VehicleType[i].LuggageCapacity+'</td><td style="display:none"> <a href="vehicle_type.html?vehicle='+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'" class="btn btn-primary btn-xs submit_popup" id="rate_'+responseObj.VehicleTypes.VehicleType[i].VehTypeImg1+'" >Rates</a></td></tr>';
							}
							else
							{
								responseHTML+='<tr id="vehicle_type_1994"> <td>'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</td><td>'+responseObj.VehicleTypes.VehicleType[i].PassengerCapacity+'</td><td><strong>'+responseObj.VehicleTypes.VehicleType[i].VehTypeTitle+'</strong> <br><img src="'+defaultImagePath+'" width="174" height="120" class="img-polaroid"> </td><td>'+responseObj.VehicleTypes.VehicleType[i].LuggageCapacity+'</td><td style="display:none"> <a href="vehicle_type.html??vehicle='+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'"" class="btn btn-primary btn-xs submit_popup" id="rate_'+responseObj.VehicleTypes.VehicleType[i].VehTypeImg1+'">Rates</a></td></tr>';
							}
						}					
					}
			    }

						
				$('#setCarDetail').html(responseHTML);
				$('#refresh_overlay').css("display","none");
		     }else{

                   $('#setCarDetail').html('');
                   alert('LimoanyWhere is in off mode please set to it on.');
                   $('#refresh_overlay').css("display","none"); 

		     }		
			}
		});
	}