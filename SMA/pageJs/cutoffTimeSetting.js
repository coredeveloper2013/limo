/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: cuttoff time setting
  Author: Mylimoproject
---------------------------------------------*/

/*create a class for the cutoff setting page*/
var cutoffTimeClass={

   /*set the php file setup path for web service*/
  _SERVICEPATHServer:"phpfile/cutoffTimeSetting_client.php",

  /*---------------------------------------------
    Function Name: pageLoadSettingFunction
    Input Parameter:All the form data
    return:json data
 ---------------------------------------------*/
   pageLoadSettingFunction:function(){
       $('#refresh_overlay').css("display","block");
       $('#cutoffSettingForm').on("submit",function(eve){
  	    eve.preventDefault();
        var service_type = $('#service_type option:selected');
        var hhTime=$('#hhTime').val();
        var mmTime=$('#mmTime').val();
  	    var save_as=$('#save_as').val();
  	    var cutoffMessage=$('#cutoff_comment').val();
  	    var networkCheckBox=$('.networkCutoffTime').is(":checked")?1:0;
  	    var service_typeObject = [];
  	    var userInfo=window.localStorage.getItem("companyInfo");
  	    var userInfoObj=userInfo;	
        if(typeof(userInfo)=="string")
  	    {
  		      userInfoObj=JSON.parse(userInfo);
  	    }	
        $(service_type).each(function(index, service_type){
            service_typeObject .push($(this).val());
        });

        if(service_typeObject=='')
  		  {
    			 $('#refresh_overlay').css("display","none");
    			 alert("Please Select At Least One service type ");
    			 return false;
  		  }
        service_typeObject=JSON.stringify(service_typeObject);
  			if($('#save_rate').val()=='Update')
  			{
            $('#refresh_overlay').css("display","block");
  				  var rowId=$('#save_rate').attr("seq");
  					$.ajax({
  					    url: cutoffTimeClass._SERVICEPATHServer,
  					    type: 'POST',
  				   	  data: "action=updateCutoffTime&service_type="+service_typeObject+"&save_as="+save_as+"&cutoffMessage="+cutoffMessage+"&networkCheckBox="+networkCheckBox+"&user_id="+userInfoObj[0].id+"&rowId="+rowId+"&hhTime="+hhTime+"&mmTime="+mmTime,
  					    success: function(response) {
      						  var responseObj=response;
        						if(typeof(response)=="string")
        						{
        							 responseObj=JSON.parse(response);
        						}
        						if(responseObj.code==1007)
        						{
        							 alert("This setting already exist");
        						}
        						else
        						{
        							 alert("Update Successfully");
        							 $('#cutoffSettingForm')[0].reset();
        							 $("#service_type").multiselect('refresh');
        							 cutoffTimeClass.getCutoffSetting();
        						}
                   $('#refresh_overlay').css("display","none");
    			     }
               
    		   });
  		  }
    		else
    		{
          $('#refresh_overlay').css("display","block");
    			$.ajax({
    					url: cutoffTimeClass._SERVICEPATHServer,
    					type: 'POST',
    				  data: "action=setCutoffTime&service_type="+service_typeObject+"&save_as="+save_as+"&cutoffMessage="+cutoffMessage+"&networkCheckBox="+networkCheckBox+"&user_id="+userInfoObj[0].id+"&mmTime="+mmTime+"&hhTime="+hhTime,
    					success: function(response) {
      					  var responseObj=response;
      						if(typeof(response)=="string")
      						{
      								responseObj=JSON.parse(response);
      						}
      						if(responseObj.code==1007)
      						{
      							  alert("This setting already exist");
                      $('#refresh_overlay').css("display","none");
      						}
      						else
      						{
      							  alert("save successfully");
      							  $('#cutoffSettingForm')[0].reset();
      							  $("#service_type").multiselect('refresh');
                      $('#refresh_overlay').css("display","none");
      							  cutoffTimeClass.getCutoffSetting();
      						}
                 $('#refresh_overlay').css("display","none");  
    				  }
             
    		  });
  	    }
     });
  },

    /*---------------------------------------------
      Function Name: getCutoffSetting
      Input Parameter:All the form data
      return:json data
    ---------------------------------------------*/ 
      getCutoffSetting:function(){
          $('#refresh_overlay').css("display","block");  
          var userInfo=window.localStorage.getItem("companyInfo");
    			var userInfoObj=userInfo;	
    			if(typeof(userInfo)=="string")
    			{
    				userInfoObj=JSON.parse(userInfo);
    			}	
        	$.ajax({
            url: cutoffTimeClass._SERVICEPATHServer,
            type: 'POST',
            data: "action=getCutoffSetting&user_id="+userInfoObj[0].id,
            success: function(response){
            	  var responseObj=response;
            		if(typeof(responseObj=="string"))
            		{
            			responseObj=JSON.parse(response);
            		}
            		var responseHtml='';
            	  if(responseObj.code==1006)
            		{
            			$.each(responseObj.data,function(index,result){
            				  var network_status="Enabled";
            					if(result.network_status==0)
            					{
            						network_status="Disabled";
            					}
                      var optionServiceType='';
                      $.each(result.service_type_array,function(index,serviceTypeValue){
                             	
                        	optionServiceType+="<option value='"+serviceTypeValue.service_type+"'>"+serviceTypeValue.service_name+"</option>";
                      })

            				  responseHtml+='<tr><td style="text-align:center">'+(parseInt(index)+1)+'</td><td style="text-align:center" >'+result.cutoff_save+'</td><td style="text-align:center" ><select>'+optionServiceType+'</select></td><td style="text-align:center" >'+result.hh_time+":"+result.mm_time+' Hr</td><td style="text-align:center" >'+network_status+'</td><td style="text-align:center" ><a class="btn btn-xs editCutoff" seq="'+result.id+'">Edit</a><a class="btn btn-xs deleteCutoff" seq="'+result.id+'">Delete</a></td></tr>';
            			});
            			$('#cutoffMatrixList').html(responseHtml);
                  $('#refresh_overlay').css("display","none");
                  /*click on the edit cut off button*/
            			$('.editCutoff').on("click",function(){
                        $('#refresh_overlay').css("display","block");
              				  var rowId=$(this).attr("seq");
              				  $('#save_rate').val("Update");
              				  $('#save_rate').attr("seq",rowId);
              	        var userInfo=window.localStorage.getItem("companyInfo");
      					        var userInfoObj=userInfo;	
              					if(typeof(userInfo)=="string")
              					{
              						userInfoObj=JSON.parse(userInfo);
              					}	

               				  $.ajax({
              					    url: cutoffTimeClass._SERVICEPATHServer,
              					    type: 'POST',
              					    data: "action=getCutoffSettingRow&user_id="+userInfoObj[0].id+"&rowId="+rowId,
              					    success: function(response){       
                          	    var responseObj=response;
                              	if(typeof(responseObj)=="string")
                              	{
                              		responseObj=JSON.parse(response);
                              	}
                          	    if(responseObj.code==1006)
                          	    {
                                     if(responseObj.data[0].hh_time==24){
                                        $('#mmTime').prop("disabled",true).val('');
                                     }else{
                                      $('#mmTime').prop("disabled",false);
                                     }
                                     $('#save_as').val(responseObj.data[0].cutoff_save);
                                     $('#hhTime').val(responseObj.data[0].hh_time);
                                     $('#mmTime').val(responseObj.data[0].mm_time);
                    		  					 $('#cutoff_comment').val(responseObj.data[0].cutoff_message);
                    		  					if(responseObj.data[0].network_status=="1")
                    		  					{
                    		  						  $('.networkCutoffTime').prop("checked",true);
                    		  					}
                    		  					else
                    		  					{
                    		  						  $('.networkCutoffTime').prop("checked",false);
                    		  					}
                      							var selectBoxArray=[];
                      							$.each(responseObj.data[0].service_type_array,function(index,responseResult){
                    		  						  selectBoxArray.push(responseResult.service_type);
                      							})
                    		        		$("#service_type").val(selectBoxArray);
                    							  $("#service_type").multiselect('refresh');
                    	        }
                              $('#refresh_overlay').css("display","none");
                          }
                      });	
                });
                   
                /*click on delete cut off button*/
            		$('.deleteCutoff').on("click",function(){
                    $('#refresh_overlay').css("display","block");  
                    var rowId=$(this).attr("seq");
                  	var confirmBox=confirm("Are you sure. you want to delete !");
                  	if(confirmBox)
                  	{
                  			$.ajax({
                           url: cutoffTimeClass._SERVICEPATHServer,
                           type: 'POST',
                           data: "action=deleteCutoff&rowId="+rowId,
                           success: function(response) {

                              	alert("delete successfully")
                              	$('#cutoffSettingForm')[0].reset()
                              	$("#service_type").multiselect('refresh');
                              	cutoffTimeClass.getCutoffSetting();
                            }
                        });

                  	}
                          		
                });
            }

         }

      });

    },

 
   /*---------------------------------------------
      Function Name: getServiceTypes
      Input Parameter:All the form data
      return:json data
    ---------------------------------------------*/ 
    getServiceTypes:function()
    {
        var userInfo=window.localStorage.getItem("companyInfo");
        var userInfoObj=userInfo;
        if(typeof(userInfo)=="string"){

             userInfoObj=JSON.parse(userInfo);

        }
        var serviceTypeData = [];
        $.ajax({
              url : "phpfile/service_type.php",
              type : 'post',
              data :'action=GetServiceTypes&user_id='+userInfoObj[0].id,
              dataType : 'json',
              success : function(data){

                  if(data.ResponseText == 'OK'){
                        var ResponseHtml='';
                        $.each(data.ServiceTypes.ServiceType, function( index, result){
                            ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
                             
                        });
                           
                        $('#service_type').html(ResponseHtml);
                  }
                  $("#service_type").multiselect('destroy');
                  $('#service_type').multiselect({
                          maxHeight: 200,
                          buttonWidth: '178px',
                          includeSelectAllOption: true
                  });
              }
        });
    }
};

/*call the function on page load */
cutoffTimeClass.getServiceTypes();
cutoffTimeClass.pageLoadSettingFunction();
cutoffTimeClass.getCutoffSetting();

