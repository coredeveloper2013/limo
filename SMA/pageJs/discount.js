/*---------------------------------------------
    Template Name: Mylimoproject
    Page Name: Discount
    Author: Mylimoproject
---------------------------------------------*/
var discountDbClass={
	/*set a web service path for webservice*/
	_Serverpath:"phpfile/discount_client.php",
	_SERVICEPATH2:"phpfile/service.php",
    _SERVICEPATHSma:"phpfile/sma_client.php",

    /*---------------------------------------------
       Function Name: getVehicleType()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/ 
    getVehicleType:function(){
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
		if(typeof(getLocalStoragevalue)=="string"){
	   		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}

	    var getuserId=window.localStorage.getItem("companyInfo");
	    if(typeof(getuserId)=="string")
		{
		   getuserId=JSON.parse(getuserId);
		}
	   
		$.ajax({
			url: discountDbClass._SERVICEPATH2,
			type: 'POST',
			data: "action=GetVehicleTypes&limoApiID="+getLocalStoragevalue.limo_any_where_api_id+"&user_id="+getuserId[0].id+"&limoApiKey="+getLocalStoragevalue.limo_any_where_api_key,
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}

				responseHTML='';
	            if(responseObj.VehicleTypes.VehicleType[0].VehTypeCode!=null){
					for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++){
						responseHTML +='<option value="'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'">'+responseObj.VehicleTypes.VehicleType[i].VehTypeCode+'</option>';
					}
				}	

				$('#apply_vehicle_type').html(responseHTML);
				setTimeout(function(){
					$("#apply_vehicle_type").multiselect('destroy');
					$('#apply_vehicle_type').multiselect({
		                maxHeight: 200,
		                buttonWidth: '155px',
		                includeSelectAllOption: true
		            });
				},400);
			}
		});
	},

	/*---------------------------------------------
       Function Name: getSMA()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/ 
	getSMA:function(){
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string"){
			userInfoObj=JSON.parse(userInfo);
		}

		$.ajax({
			url: discountDbClass._SERVICEPATHSma,
			type: 'POST',
			data: "action=getSMA&user_id="+userInfoObj[0].id,
			success: function(response) {
				var responseObj=response;
				var responseHTML='';
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);
				}	
				var responseHTML='';
                  	if(responseObj.data[0].sma_name!=null){
						for(var i=0; i<responseObj.data.length; i++){	
							responseHTML +='<option value="'+responseObj.data[i].id+'">'+responseObj.data[i].sma_name+'</option>';	
						}
               		}
				$('#apply_sma').html(responseHTML);
				setTimeout(function(){				
					$("#apply_sma").multiselect('destroy');
					$('#apply_sma').multiselect({
						maxHeight: 200,
						buttonWidth: '155px',
						includeSelectAllOption: true
					});
				},400);				
			},
			error:function(){
				alert("Some Error");}
			});
	},

	/*---------------------------------------------
       Function Name: getServiceTypes()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/ 
	getServiceTypes:function(){
		var userInfo=window.localStorage.getItem("companyInfo");
		var userInfoObj=userInfo;
		if(typeof(userInfo)=="string"){
		    userInfoObj=JSON.parse(userInfo);
		}
        
        var serviceTypeData = [];
    	$.ajax({
            url : "phpfile/service_type.php",
            type : 'post',
            data : 'action=GetServiceTypes&user_id='+userInfoObj[0].id,
            dataType : 'json',
            success : function(data){
                if(data.ResponseText == 'OK'){
	 				var ResponseHtml='';
	                $.each(data.ServiceTypes.ServiceType, function( index, result){
	                 	ResponseHtml+="<option value='"+result.SvcTypeCode+"'>"+result.SvcTypeDescription+"</option>";
	                });
	                 
	                $('#service_type').html(ResponseHtml);
				}
                $("#service_type").multiselect('destroy');
            	$('#service_type').multiselect({
	                maxHeight: 200,
	                buttonWidth: '178px',
	                includeSelectAllOption: true
            	});
      		}
      	});
 	},

 	/*---------------------------------------------
       Function Name: getDiscountCuponList()
       Input Parameter: 
       return:json data
    ---------------------------------------------*/ 
	getDiscountCuponList:function(){

		$('#refresh_overlay').css("display","block");
	 	var getUserId=window.localStorage.getItem('companyInfo');
		if(typeof(getUserId)=="string"){	
			getUserId=JSON.parse(getUserId);
		}

		var fd = new FormData();
		fd.append("action","getDiscountCuponList");
		fd.append("user_id",getUserId[0].id);

		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data:fd
		}).done(function(result){          
	        var responseObj=result;
			var responseHTML=' ';
			var responseVeh='';
			var responseSma='';
			var vehicle_code='';
			var vehicle_code_array='';
			var sma_id_array='';
			var sma_id='';								
			if(typeof(result)=="string"){
				responseObj=JSON.parse(result);
			}
			if(responseObj.data[0].id!=null){
				for(var i=0; i<responseObj.data.length; i++){
					responseVeh=0;
					responseSma=0;	
					vehicle_code = responseObj.data[i].vehicle_code;
					vehicle_code_array=vehicle_code.split(',');					
					sma_id = responseObj.data[i].sma_id;
					sma_id_array=sma_id.split(',');
					var s=0;
					for(s=0;s<vehicle_code_array.length;s++){
						if(vehicle_code_array[s]!=''){
							responseVeh +='<option selected disabled>'+vehicle_code_array[s]+'</option>';	
						}						
					}
					
					var t=0;
					for(t=0;t<sma_id_array.length;t++){
	                   if(sma_id_array[t]!='')
						{
						responseSma +='<option selected disabled>'+sma_id_array[t]+'</option>';
	                    }
					}
	                var discount_value;
	                if(responseObj.data[i].discount_type=='%'){
						discount_value=responseObj.data[i].discount_value+'%';
	                }
	                else{
	                    discount_value='$'+responseObj.data[i].discount_value;
					}
					
					responseHTML +='<tr seq="'+responseObj.data[i].id+'"><td style="text-align:center" id="discount_cuppon_name_'+responseObj.data[i].id+'">'+responseObj.data[i].name+'</td> <td style="text-align:center" id="discount_cuppon_code_'+responseObj.data[i].id+'">'+responseObj.data[i].code+'</td><td style="text-align:center" id="discoutn_value">'+discount_value+'</td><td style="text-align: left;" id="discount_cuppon_start_date_'+responseObj.data[i].id+'">'+responseObj.data[i].start_date+'</td> <td style="text-align: left;" id="discount_cuppon_end_date_'+responseObj.data[i].id+'">'+responseObj.data[i].end_date+'</td> <td style="text-align:center;width: 21%; text-align: justify;" id="promo_veh_'+responseObj.data[i].id+'" veh_code="'+vehicle_code +'"><select class="refresh_multi_select" multiple>'+responseVeh+'</select></td><td style="text-align:center;width: 21%; text-align: justify;" id="promo_sma_'+responseObj.data[i].id+'" sma_id="'+sma_id +'" ><select class="refresh_multi_select" multiple>'+responseSma+'</select></td><td style="text-align:center"><div class="as" ng-show="!rowform.$visible"> <a class="btn btn-xs rate_matrix_onedit" id=edit_promo_'+responseObj.data[i]['id']+' onclick="discountDbClass.viewDiscountCuppon('+responseObj.data[i].id+',1)">Edit</a> <a class="btn btn-xs rate_matrix_ondelete"  seq='+responseObj.data[i]['id']+' onclick="discountDbClass.deleteDiscountCuponList('+responseObj.data[i].id+')">Delete</a> </div></td></tr>';										
				} 

			}
			else{
			 	responseHTML+='<tr><td colspan="7">Data not found</td></tr>';
			}
	    	$('#discount_cuppon_list').html(responseHTML)
			setTimeout(function(){
				$(".refresh_multi_select").multiselect('destroy');
				$('.refresh_multi_select').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});
			},800);
	           
	      	$('#refresh_overlay').css("display","none");
		});
	},

	/*---------------------------------------------
       Function Name: deleteDiscountCuponList()
       Input Parameter: rowId
       return:json data
    ---------------------------------------------*/ 
	deleteDiscountCuponList:function(rowId){     
		$('#refresh_overlay').css("display","block");		      
        var fd = new FormData();
		fd.append("action","deleteDiscountCuponList");
		fd.append("row_Id",rowId);
	   	var getAns=confirm("Are you sure. you want to delete item");
       	if(getAns){

		$.ajax({
				url: discountDbClass._Serverpath,
				type: 'POST',
				processData:false,
				contentType:false,
				data: fd
			}).done(function(result){
	           		location.reload();     
				});
	   	}
	    
	    $('#refresh_overlay').css("display","none");
	},

	/*---------------------------------------------
	   Function Name: getAllUser()
	   Input Parameter: 
	   return:json data
	---------------------------------------------*/ 
	getAllUser:function(){
		var getLocalStoragevalue=window.localStorage.getItem("apiInformation");
		if(typeof(getLocalStoragevalue)=="string"){
	   		getLocalStoragevalue=JSON.parse(getLocalStoragevalue);
		}
	  	$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			data: "action=GetUsers",
			success: function(response) {
				var responseHTML='';
				var responseObj=response;
				if(typeof(response)=="string"){
					responseObj=JSON.parse(response);					
				}
				var getHtml='';
				$.each(responseObj.data,function(index,value){
					getHtml+='<option value="'+value.ac_number+'">'+value.f_name+' '+value.l_name+'</option>';

				});
				$('#allUser').html(getHtml);
				$("#allUser").multiselect('destroy');
            	$('#allUser').multiselect({
	                maxHeight: 200,
	                buttonWidth: '155px',
	                includeSelectAllOption: true
	            });				
			}
		});
	},

	/*---------------------------------------------
	   Function Name: viewDiscountCuppon()
	   Input Parameter: 
	   return:json data
	---------------------------------------------*/ 
	viewDiscountCuppon:function(rowId){     
		$('#refresh_overlay').css("display","block");
	    $('#save_rate12').html("Update");
	    $('#save_rate12').attr("seq",rowId);
	    $('#back_button').css("display","block");
	 	var viewDiscount = new FormData();
		viewDiscount.append("action","viewDiscountCuppon");
		viewDiscount.append("view_Id",rowId);
		$.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,

			data: viewDiscount
		}).done(function(result){
           	var responseObj=result;
			var responseVeh='';
			var responseSma='';
			var vehicle_code='';
			var vehicle_code_array='';
			var sma_id_array='';
			var sma_id='';
			var rate_type_array='';
			var rate_type='';
			var service_type_array='';
			var service_type='';
          	if(typeof(result)=="string"){
				responseObj=JSON.parse(result);
            }
            
			if(responseObj.data!=null && responseObj.data!="null" && responseObj.data!=undefined && responseObj.data!="undefined" &&
			responseObj.data!=""){
                vehicle_code = responseObj.data[0].vehicle_code;
				vehicle_code_array=vehicle_code.split(',');			
				sma_id = responseObj.data[0].sma_id;
				sma_id_array=sma_id.split(',');
				rate_type = responseObj.data[0].apply_on;
				rate_type_array=rate_type.split(',');
				service_type = responseObj.data[0].apply_service;
				service_type_array=service_type.split(',');
	           	var start_date=responseObj.data[0].start_date;
          	 	var start_date=start_date.split('-');
	           	var start_date=start_date[1]+"/"+start_date[2]+"/"+start_date[0];
               	var end_date=responseObj.data[0].end_date;
	           	var end_date=end_date.split('-');
	           	var end_date=end_date[1]+"/"+end_date[2]+"/"+end_date[0];
	           	var end_time =responseObj.data[0].end_time;
	            end_time=end_time.split('-');
	            var end_time1=end_time[1]+"/"+end_time[2]+"/"+end_time[0];
	            var start_time =responseObj.data[0].start_time;
	            start_time=start_time.split('-');
	            var start_time1=start_time[1]+"/"+start_time[2]+"/"+start_time[0];

              	$("#cuppon_name").val(responseObj.data[0].name);
              	$("#cuppon_code").val(responseObj.data[0].code);
              	$("#cuppon_value").val(responseObj.data[0].discount_value);
              	$("#cupon_discount_type").val(responseObj.data[0].discount_type);
              	$("#start_date").val(start_date);
              	$("#endCalender").val(end_date);
              	$("#start_time").val(start_time1);
              	$("#end_time").val(end_time1);
              	$("#rate_type").val(rate_type_array);
              	$("#service_type").val(service_type_array);
              	$("#apply_vehicle_type").val(vehicle_code_array);
              	$("#apply_sma").val(sma_id_array);                   
                $("#apply_vehicle_type").multiselect('destroy');
				$('#apply_vehicle_type').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});
                $("#apply_sma").multiselect('destroy');
				$('#apply_sma').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});
				$("#service_type").multiselect('destroy');
				$('#service_type').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});
				$("#rate_type").multiselect('destroy');
				$('#rate_type').multiselect({
					maxHeight: 200,
					buttonWidth: '155px',
					includeSelectAllOption: true
				});

				if(responseObj.data[0].promo_pref=="2"){
					$(".promoCode").prop('checked', true);
                    $('.promocodeManageAll').css('display','none');                      
                }
                else if(responseObj.data[0].promo_pref=="1"){
					$(".autoApply").prop('checked', true);
					$('.promocodeManageAll').css('display','block');
                    if(responseObj.data[0].allUserSelectUser.length>1){
                      	$('.applyAutoPromoUser').prop("checked",true);
                      	var getHtml=[];
                      	$.each(responseObj.data[0].allUserSelectUser,function(index,value){
                  			getHtml.push(value[0].ac_number);
                  		})
                  		$('#allUser').val(getHtml);
                  		$("#allUser").multiselect('destroy');
				        $('#allUser').multiselect({
							maxHeight: 200,
							buttonWidth: '155px',
							includeSelectAllOption: true
						});
                     	$('.allUserMultiSelect').css("visibility","visible");
						$('.applyAutoPromoUser,.applyAutoPromo').on("change",function(){
                  			if($('.applyAutoPromoUser').is(":checked")){
                 				$('.allUserMultiSelect').css("visibility","visible");
                   			}
                  			else{
			                  	$('#allUser').val('');
			                    $("#allUser").multiselect('destroy');
								$('#allUser').multiselect({
									maxHeight: 200,
									buttonWidth: '155px',
									includeSelectAllOption: true
								});

                    			$('.allUserMultiSelect').css("visibility","hidden");
                  			}                 
               			});

					}
                  	else{
                     	$('.applyAutoPromoUser,.applyAutoPromo').on("change",function(){
							if($('.applyAutoPromoUser').is(":checked")){
                     			$('.allUserMultiSelect').css("visibility","visible");
                          	}
                 	 		else{
			                  	$('#allUser').val('');
			                    $("#allUser").multiselect('destroy');
								$('#allUser').multiselect({
									maxHeight: 200,
									buttonWidth: '155px',
									includeSelectAllOption: true
								});

                    			$('.allUserMultiSelect').css("visibility","hidden");
                  			}
              			});

                  		if(responseObj.data[0].is_all_user=="All"){
                  			$('#applyAutoPromo').prop("checked",true);                  				
                            $('.allUserMultiSelect').css("visibility","hidden");                                        
                      	}
                      	else if(responseObj.data[0].is_all_user=="AllUser"){
                            $('#applyAutoPromoUser').prop("checked",true);
                            var getHtml=[];
                      		$.each(responseObj.data[0].allUserSelectUser,function(index,value){
                      			getHtml.push(value[0].ac_number);
                      		})
                      		$('#allUser').val(getHtml);
                      		$("#allUser").multiselect('destroy');
							$('#allUser').multiselect({
								maxHeight: 200,
								buttonWidth: '155px',
								includeSelectAllOption: true
							});
			                $('.allUserMultiSelect').css("visibility","visible");
                      	}
                    }
				}

              	if(responseObj.data[0].is_combine_discount=="1"){
                    $('#is_combine_discount').prop('checked', true);
              	}
              	$('#refresh_overlay').css("display","none");
			}	
				setTimeout(function(){
					$("html, body").animate({ scrollTop: 0 }, "slow");	
					$('#refresh_overlay').css("display","none");
				},200);
			                 
		});
        
        
	}
}

/* on click function on back button*/
$('#back_button').on("click",function(){
	location.reload(true);
});	

/*submit the discount form*/
$('#discount_form').on("submit",function(event){
	$('#refresh_overlay').css("display","block");
	event.preventDefault();
    var row_id = $('#save_rate12').attr('seq');
    var start_date=$('#start_date').val();
	var endCalender=$('#endCalender').val();
	var newDateStart=new Date(start_date);
	var endDateStart=new Date(endCalender);
	if(newDateStart>endDateStart){
		alert("Service Date End Date is greater than start time");
		$('#refresh_overlay').css("display","none");
		return 0;
	}
	
	var start_time=$('#start_time').val();
	var end_time=$('#end_time').val();
	var start_time=new Date(start_time);
	var end_time=new Date(end_time);
	if(start_time>end_time){
		alert("Booking Date End Date is greater than start time");
		$('#refresh_overlay').css("display","none");
		return 0;
	}
    var service_date=$('#start_date').val(); 
    var booking_date=$('#start_time').val();

    var service_start_date=new Date(service_date);
    var booking_start_date=new Date(booking_date);
    var d = new Date();
    var start_date=d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear();
   	var today_date= new Date(start_date);
    if(row_id){
		var edit_row_id = new FormData($('#discount_form')[0]);
      	edit_row_id.append("action","updateCupponData");
      	edit_row_id.append('edit_row_id',row_id);
		var selectedVehicle = $('#apply_vehicle_type option:selected');
		var selectedVehicleObject = [];
		$(selectedVehicle).each(function(index, selectedVehicle){
			selectedVehicleObject .push($(this).val());
		});
        edit_row_id.append("apply_vehicle_type",selectedVehicleObject);
		var selectedSma = $('#apply_sma option:selected');
		var selectedSmaObject = [];
		$(selectedSma).each(function(index, selectedSma){
			selectedSmaObject .push($(this).val());
		});
		edit_row_id.append("apply_sma",selectedSmaObject);
		var selectedRate = $('#rate_type option:selected');
		var selectedRateObject = [];
		$(selectedRate).each(function(index, selectedRate){
			selectedRateObject .push($(this).val());
		});
		edit_row_id.append("rate_type",selectedRateObject);
		var selectedService = $('#service_type option:selected');
		var selectedServiceObject = [];
		$(selectedService).each(function(index, selectedService){
			selectedServiceObject .push($(this).val());
		});
		edit_row_id.append("service_type",selectedServiceObject);
        var allUserSelectOption = $('#allUser option:selected');
		var allUserArray=[];
       	$(allUserSelectOption).each(function(index,selectedValue){
       		allUserArray.push($(this).val());
   		});
       	edit_row_id.append("allUser",allUserArray);
        $.ajax({
			url: discountDbClass._Serverpath,
			type: 'POST',
			processData:false,
			contentType:false,
			data: edit_row_id
	     	}).done(function(result){
					result=JSON.parse(result);
		     		if(result.code==1005){
						alert("Please Enter Unique Promo code or Unique Promo name");
		     		}
		     		else{
						alert("successfully updated");
		     			location.reload();		
		     		}
			     	$('#refresh_overlay').css("display","none");
		     	});
		}
		else{
			var fd = new FormData($('#discount_form')[0]);
			fd.append("action","setDiscount");
			var getUserId=window.localStorage.getItem('companyInfo');
			var selectedVehicle = $('#apply_vehicle_type option:selected');
           	var selectedVehicleObject = [];
	           	$(selectedVehicle).each(function(index, selectedVehicle){
	            selectedVehicleObject .push($(this).val());
	        });
	        fd.append("apply_vehicle_type",selectedVehicleObject);
			var allUserSelectOption = $('#allUser option:selected');
			var allUserArray=[];
	           $(allUserSelectOption).each(function(index,selectedValue){
	           		allUserArray.push($(this).val());
	           });
            fd.append("allUser",allUserArray);
			var selectedSma = $('#apply_sma option:selected');
			var selectedSmaObject = [];
        	$(selectedSma).each(function(index, selectedSma){
	            selectedSmaObject .push($(this).val());
	        });
        	fd.append("apply_sma",selectedSmaObject);
        	var selectedRate = $('#rate_type option:selected');
			var selectedRateObject = [];
			$(selectedRate).each(function(index, selectedRate){
				selectedRateObject .push($(this).val());
			});
			fd.append("rate_type",selectedRateObject);
			var selectedService = $('#service_type option:selected');
			var selectedServiceObject = [];
			$(selectedService).each(function(index, selectedService){
				selectedServiceObject .push($(this).val());
			});
			fd.append("service_type",selectedServiceObject);
			if(typeof(getUserId)=="string"){
				getUserId=JSON.parse(getUserId);
			}

       		fd.append("user_id",getUserId[0].id);
	        $.ajax({
				url: discountDbClass._Serverpath,
				type: 'POST',
				processData:false,
				contentType:false,
				data: fd
			}).done(function(result){
			  		result=JSON.parse(result);
			   		if(result.code==1006){
						alert("Enter promo code unique or name ");
			  	 	}
			   		else{
			   	 		alert("successfully Added");
			   			location.reload();
			   		}
	          		$('#refresh_overlay').css("display","none");
				});
	        	$('#refresh_overlay').css("display","none");
		}
	});

discountDbClass.getVehicleType();
discountDbClass.getAllUser();
discountDbClass.getServiceTypes();
discountDbClass.getSMA();
discountDbClass.getDiscountCuponList();
