
/*---------------------------------------------
 Template Name: Mylimoproject
 Page Name: Hourly Setup
 Author: Mylimoproject
 ---------------------------------------------*/

/*define a class name getServiceVehicleType for this service*/
var getServiceVehicleType = {

    /*define the php file path for calling the web service */
    _SERVICEPATH: "phpfile/service.php",
    _SERVICEPATHSma: "phpfile/sma_client.php",
    _SERVICEPATHBlackOut: "phpfile/blackout_client.php",

    /*---------------------------------------------
     Function Name: getHrlySma
     Input Parameter:user id 
     return:list of SMA
     ---------------------------------------------*/
    getHrlySma: function () {

        var userInfo = window.localStorage.getItem("companyInfo");
        
        var userInfoObj = userInfo;

        if (typeof (userInfo) == "string") {

            userInfoObj = JSON.parse(userInfo);
        }

        $.ajax({
            url: getServiceVehicleType._SERVICEPATHSma,
            type: 'POST',
            data: "action=getSMA&user_id=" + userInfoObj[0].id,
            success: function (response) {
                var responseObj = response;
                var responseHTML = '';
                if (typeof (response) == "string") {
                    responseObj = JSON.parse(response);
                }
                var responseHTML = '';
                for (var i = 0; i < responseObj.data.length; i++)
                {
                    if (responseObj.data[i].id != null) {
                        responseHTML += '<option value="' + responseObj.data[i].id + '">' + responseObj.data[i].sma_name + '</option>';
                    }
                }
                $('#hrlySmaValue').html(responseHTML);
                setTimeout(function () {
                    $("#hrlySmaValue").multiselect('destroy');
                    $('#hrlySmaValue').multiselect({
                        maxHeight: 116,
                        buttonWidth: '155px',
                        includeSelectAllOption: true

                    });

                }, 400);

            },
            error: function () {
                alert("Some Error");
            }
        });

    },

    /*---------------------------------------------
     Function Name: getBlacourEvent
     Input Parameter:user id 
     return:list of Blackout Event
     ---------------------------------------------*/
    getBlacourEvent: function () {

        var userInfo = window.localStorage.getItem("companyInfo");
        var userInfoObj = userInfo;
        if (typeof (userInfo) == "string")
        {
            userInfoObj = JSON.parse(userInfo);
        }
        $.ajax({
            url: getServiceVehicleType._SERVICEPATHBlackOut,
            type: 'POST',
            data: "action=getRateMatrixList&user_id=" + userInfoObj[0].id,
            success: function (response) {

                var responseObj = response;
                if (typeof (response) == "string")
                {
                    responseObj = JSON.parse(response);
                }

                var finalHtml = '';
                $.each(responseObj.data, function (index, resultfinal) {

                    finalHtml += '<option value=' + resultfinal.id + '>' + resultfinal.value + '</option>';
                })

                $('#eventSelectvalue').html(finalHtml)
                $('#eventSelectvalue option').prop('disabled', true);
                $("#eventSelectvalue").multiselect('refresh');

            },
            error: function () {
                alert("Some Error");
                $('#refresh_overlay').css("display", "none");
            }

        });

    },

    /*---------------------------------------------
     Function Name: getBlacourEvent
     Input Parameter:user id 
     return:list of Blackout Event
     ---------------------------------------------*/
    getVehicle: function ()
    {
        $('#refresh_overlay').css("display", "block");
        var getLocalStoragevalue = window.localStorage.getItem("apiInformation");
        if (typeof (getLocalStoragevalue) == "string") {

            getLocalStoragevalue = JSON.parse(getLocalStoragevalue);

        }
        var getuserId = window.localStorage.getItem("companyInfo");
        if (typeof (getuserId) == "string")
        {
            getuserId = JSON.parse(getuserId);
        }

        $.ajax({
            url: getServiceVehicleType._SERVICEPATH,
            type: 'POST',
            data: "action=GetVehicleTypes&limoApiID=" + getLocalStoragevalue.limo_any_where_api_id + "&user_id=" + getuserId[0].id + "&limoApiKey=" + getLocalStoragevalue.limo_any_where_api_key,
            success: function (response) {
                var k = 1;
                var responseHTML = '';
                var responseObj = response;
                if (typeof (response) == "string")
                {
                    responseObj = JSON.parse(response);
                }

                var getFinal = [];
                for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++) {

                    getFinal[i] = responseObj.VehicleTypes.VehicleType[i].PassengerCapacity + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '@' + responseObj.VehicleTypes.VehicleType[i].VehTypeId;

                }
                getFinal.sort();
                for (var j = 0; j < getFinal.length; j++)
                {
                    var carsplitResult = getFinal[j].split('@');
                    for (var i = 0; i < responseObj.VehicleTypes.VehicleType.length; i++)
                    {
                        if (carsplitResult[1] == responseObj.VehicleTypes.VehicleType[i].VehTypeId)
                        {
                            if (typeof (responseObj.VehicleTypes.VehicleType[i].VehTypeImg1) != "undefined" && typeof (responseObj.VehicleTypes.VehicleType[i].VehTypeImg1) != undefined)
                            {
                                responseHTML += '<div class="row"> <div class="col-xs-1 col-sm-1 col-md-1" style="width:4%"> <input type="checkbox" class="mnd_check" name="peakHourCheck" value="' + k + '"></div> <div style="width:10%; margin-top:8px" class="col-xs-2 col-sm-2 col-md-2 ' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + ' dayName' + k + '" seq=' + k + ' >' + responseObj.VehicleTypes.VehicleType[i].VehTypeCode + '</div> <div class="col-xs-3 col-sm-3 col-md-3"> <div class="input-group"> <input type="text" class="form-control input-small input-group-addon stdhrs' + k + '" value="" disabled style="width: 21%;"> <input type="text" class="form-control input-small input-group-addon dschrscm dschrs' + k + '" seq=' + k + ' value="" disabled style="width: 21%;"> <input type="text" class="form-control input-small input-group-addon sdchrscm sdchrs' + k + '"  seq=' + k + '  value="" disabled style="width: 21%;"> </div> </div> <div class="col-xs-3 col-sm-3 col-md-3"> <div class="input-group"> <input type="text" class="form-control input-small input-group-addon stdprice' + k + '" value="" disabled style="width: 21%;"> <input type="text" class="form-control input-small input-group-addon  dschrscm dscprice' + k + '" seq=' + k + ' value="" disabled style="width: 21%;"> <input type="text" class="form-control input-small input-group-addon sdchrscm sdcprice' + k + '" seq=' + k + ' value="" disabled style="width: 21%;"> </div> </div> </div>';
                                k++;
                            }


                        }

                    }
                }

                $('.addVehicleData').html(responseHTML + '<br><br>');
                $('#refresh_overlay').css("display", "none");
                $('.dschrscm').keyup(function () {
                    var getSeqLocal = $(this).attr("seq");
                    var getValue = $('.dschrs' + getSeqLocal).val();
                    var getPrice = $('.dscprice' + getSeqLocal).val();
                    if (getValue != '' || getPrice != '')
                    {
                        $('.dschrs' + getSeqLocal).prop("required", "true");
                        $('.dscprice' + getSeqLocal).prop("required", "true");

                    } else
                    {
                        $('.dschrs' + getSeqLocal).removeAttr("required");
                        $('.dscprice' + getSeqLocal).removeAttr("required");

                    }


                });

                $('.sdchrscm').keyup(function () {
                    var getSeqLocal = $(this).attr("seq");
                    var getValue = $('.sdchrs' + getSeqLocal).val();
                    var getPrice = $('.sdcprice' + getSeqLocal).val();
                    if (getValue != '' || getPrice != '')
                    {
                        $('.sdchrs' + getSeqLocal).prop("required", "true");
                        $('.sdcprice' + getSeqLocal).prop("required", "true");
                    } else
                    {
                        $('.sdchrs' + getSeqLocal).removeAttr("required").val('');
                        $('.sdcprice' + getSeqLocal).removeAttr("required").val('');
                    }

                });

                $('.mnd_check').on("change", function () {

                    var getValue = $(this).val();
                    if ($(this).is(":checked")) {

                        $('.stdhrs' + getValue).removeAttr("disabled").prop("required", "true");
                        $('.dschrs' + getValue).removeAttr("disabled");
                        $('.sdchrs' + getValue).removeAttr("disabled");
                        $('.stdprice' + getValue).removeAttr("disabled").prop("required", "true");
                        $('.dscprice' + getValue).removeAttr("disabled");
                        $('.sdcprice' + getValue).removeAttr("disabled");

                    } else
                    {
                        $('.stdhrs' + getValue).attr("disabled", "true").removeAttr("required").val('');
                        $('.dschrs' + getValue).attr("disabled", "true").removeAttr("required").val('');
                        $('.sdchrs' + getValue).attr("disabled", "true").removeAttr("required").val('');
                        $('.stdprice' + getValue).attr("disabled", "true").removeAttr("required").val('');
                        $('.dscprice' + getValue).attr("disabled", "true").removeAttr("required").val('');
                        $('.sdcprice' + getValue).attr("disabled", "true").removeAttr("required").val('');
                    }


                });


            }
        });

    }

};
$('#refresh_overlay').css("display", "block");

/*function to call on page load*/
getServiceVehicleType.getVehicle();
getServiceVehicleType.getBlacourEvent();
getServiceVehicleType.getHrlySma();



/* page edit js start here */


/*declare a retesetup final class*/
var rateSetupFinalClass = {
    /*declare a php file path to call web service*/
    _SERVICEPATH: "phpfile/hourly_client.php",
    /*---------------------------------------------
     Function Name: editSpecialHourly
     Input Parameter:rowId
     return:json data
     ---------------------------------------------*/
    editSpecialHourly: function (rowId) {

        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + rowId,
            dataType: 'json',
            success: function (response)

            {
                $('#refresh_overlay').css("display", "none");
                $('#hrly_rate_name').val(response.data[0].hourly_name);
                if (response.data[0].is_miles_set == 0)
                {
                    $('.setRadiusCheckbox').prop("checked", "true");
                    $('.setRadiusValuemiles').val(response.data[0].radius_miles).removeAttr("disabled").prop("required", "true");
                    $('.setRadiusValuehour').val(response.data[0].radius_msg).removeAttr("disabled").prop("required", "true");
                } else{

                    $('.setRadiusCheckbox').removeAttr("checked");
                    $('.setRadiusValuemiles').attr("disabled", "true").removeAttr("required").val('');
                    
                    $('.setRadiusValuehour').attr("disabled", "true").removeAttr("required").val('');

                }

                if (response.data[0].is_black_out == 0)
                {
                    $('.setblacoutCheckout').attr('checked', true);
                    $('.setblacoutValuecuthour').val(response.data[0].cut_off_time).removeAttr("disabled").prop("required", "true");
                    $('.setblacoutValueincreasehour').val(response.data[0].increase_hrs).removeAttr("disabled").prop("required", "true");
                    $('.setblacoutValueincreaserate').val(response.data[0].increase_rate).removeAttr("disabled").prop("required", "true");
                    $('.setblacoutValueincreaserateType').val(response.data[0].rate_type).removeAttr("disabled").prop("required", "true");
                    $('#eventSelectvalue option').removeAttr('disabled');
                } else
                {
                    $('.setblacoutCheckout').attr('checked', false);
                    $('.setblacoutValuecuthour').attr("disabled", "true").removeAttr("required").val('');
                    $('.setblacoutValueincreasehour').attr("disabled", "true").removeAttr("required").val('');
                    $('.setblacoutValueincreaserate').attr("disabled", "true").removeAttr("required").val('');
                    $('.setblacoutValueincreaserateType').attr("disabled", "true").removeAttr("required").val('');
                    $('#eventSelectvalue option').prop('disabled', true);
                    $('#eventSelectvalue').val('');
                    $("#eventSelectvalue").multiselect('refresh');
                }

                $('.hrlySetup').show().val('update');
                $('.hrlySetup').attr('seq', response.data[0].id);
                $('#backBtnBack').show();

                if (response.data[0].is_sma_default == 1)
                {
                    $('#checkSetDefault').prop("checked", "true");

                } else
                {
                    $('#checkSetDefault').removeAttr("checked");
                    var hourlyArray = [];
                    $.each(response.data[0].sma_info, function (index, finalSmaResult) {
                        hourlyArray.push(finalSmaResult.sma_id);
                    })
                    $("#hrlySmaValue").val(hourlyArray);
                    $("#hrlySmaValue").multiselect('refresh');

                }
                var hourlyArray = [];
                $.each(response.data[0].event_info, function (index, finalSmaResult) {

                    if (finalSmaResult != null)
                    {
                        hourlyArray.push(finalSmaResult.event_id);
                    }
                });
                $("#eventSelectvalue").val(hourlyArray);
                $("#eventSelectvalue").multiselect('refresh');
                $('.mnd_check').removeAttr("checked");
                var i = 1;
                $('.mnd_check').each(function () {

                    $('.stdhrs' + i).attr("disabled", "true").removeAttr("required").val('');
                    $('.dschrs' + i).attr("disabled", "true").removeAttr("required").val('');
                    $('.dscprice' + i).attr("disabled", "true").removeAttr("required").val('');
                    $('.stdprice' + i).attr("disabled", "true").removeAttr("required").val('');
                    $('.sdchrs' + i).attr("disabled", "true").removeAttr("required").val('');
                    $('.sdcprice' + i).attr("disabled", "true").removeAttr("required").val('');
                    i++;

                });


                $.each(response.data[0].vehicle_info, function (index, finalSmaResult) {

                    var getKey = $('.' + finalSmaResult.vehicle_code).attr('seq');
                    $('[value=' + getKey + ']').prop("checked", true);
                    $('.stdhrs' + getKey).val(finalSmaResult.std_hrs).removeAttr("disabled").prop("required", "true");
                    $('.dschrs' + getKey).val(finalSmaResult.dsc_hrs).removeAttr("disabled").prop("required", "true");
                    $('.dscprice' + getKey).val(finalSmaResult.dsc_price).removeAttr("disabled").prop("required", "true");
                    $('.stdprice' + getKey).val(finalSmaResult.std_price).removeAttr("disabled").prop("required", "true");
                    $('.sdchrs' + getKey).val(finalSmaResult.sdc_hrs).removeAttr("disabled").prop("required", "true");
                    $('.sdcprice' + getKey).val(finalSmaResult.sdc_price).removeAttr("disabled").prop("required", "true");

                });

            }
        });
    },

    /*---------------------------------------------
     Function Name: getService
     Input Parameter:user id 
     return:list of service type
     ---------------------------------------------*/
    viewSpecialHourly: function (rowId) {
        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + rowId,
            dataType: 'json',
            success: function (response)
            {
                $('#refresh_overlay').css("display", "none");
                $('#hrly_rate_name').val(response.data[0].hourly_name);

                if (response.data[0].is_miles_set == 0)
                {
                    $('.setRadiusCheckbox').attr("checked", "true");
                    $('.setRadiusValuemiles').attr("disabled", "false");
                    $('.setRadiusValuehour').attr("disabled", "false");
                    $('.setRadiusValuemiles').val(response.data[0].radius_miles);
                    $('.setRadiusValuehour').val(response.data[0].radius_msg);
                } else
                {
                    
                    $('.setRadiusCheckbox').removeAttr("checked");
                    $('.setRadiusValuemiles').val('');
                    $('.setRadiusValuehour').val('');
                }

                if (response.data[0].is_black_out == 0)
                {
                    $('.setblacoutCheckout').attr('checked', true);
                    $('.setblacoutValuecuthour').val(response.data[0].cut_off_time);
                    $('.setblacoutValueincreasehour').val(response.data[0].increase_hrs);
                    $('.setblacoutValueincreaserate').val(response.data[0].increase_rate);
                    $('.setblacoutValueincreaserateType').val(response.data[0].rate_type);
                } else
                {
                    $('.setblacoutCheckout').attr('checked', false);
                    $('.setblacoutValuecuthour').val("");
                    $('.setblacoutValueincreasehour').val("");
                    $('.setblacoutValueincreaserate').val("");
                    $('.setblacoutValueincreaserateType').val("");

                }
                if (response.data[0].is_sma_default == 1)
                {
                    $('#checkSetDefault').prop("checked", "true");

                } else
                {

                    $('#checkSetDefault').removeAttr("checked");
                    var hourlyArray = [];
                    $.each(response.data[0].sma_info, function (index, finalSmaResult) {
                        hourlyArray.push(finalSmaResult.sma_id);
                    });
                    $("#hrlySmaValue").val(hourlyArray);
                    $("#hrlySmaValue").multiselect('refresh');
                }

                var hourlyArray = [];
                $.each(response.data[0].event_info, function (index, finalSmaResult) {
                    hourlyArray.push(finalSmaResult.event_id);
                });

                $("#eventSelectvalue").val(hourlyArray);
                $("#eventSelectvalue").multiselect('refresh');
                $('.mnd_check').removeAttr("checked");
                var i = 1;
                $('.mnd_check').each(function () {
                    $('.stdhrs' + i).val('');
                    $('.dschrs' + i).val('');
                    $('.dscprice' + i).val('');
                    $('.stdprice' + i).val('');
                    $('.sdchrs' + i).val('');
                    $('.sdcprice' + i).val('');
                    i++;
                });
                $('.hrlySetup').hide();
                $('#backBtnBack').show();
                $.each(response.data[0].vehicle_info, function (index, finalSmaResult) {

                    var getKey = $('.' + finalSmaResult.vehicle_code).attr('seq');
                    $('[value=' + getKey + ']').prop("checked", true);
                    $('.stdhrs' + getKey).val(finalSmaResult.std_hrs);
                    $('.dschrs' + getKey).val(finalSmaResult.dsc_hrs);
                    $('.dscprice' + getKey).val(finalSmaResult.dsc_price);
                    $('.stdprice' + getKey).val(finalSmaResult.std_price);
                    $('.sdchrs' + getKey).val(finalSmaResult.sdc_hrs);
                    $('.sdcprice' + getKey).val(finalSmaResult.sdc_price);

                });

            }
        });
    },

    /*---------------------------------------------
     Function Name: deleteHourlyRate
     Input Parameter:rowID 
     return:json data
     ---------------------------------------------*/
    deleteHourlyRate: function (rowID)
    {
        $('#refresh_overlay').css("display", "none");
        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + rowID,
            dataType: 'json',
            success: function (response)
            {
                rateSetupFinalClass.getHourlyRate();
            }
        });
    },

    /*---------------------------------------------
     Function Name: getHourlyRate
     Input Parameter:getFullJson 
     return:list of hourly rate
     ---------------------------------------------*/
    getHourlyRate: function ()
    {

        $("#refresh_overlay").css('display', 'block');
        var userInfo = window.localStorage.getItem("companyInfo");
        var userInfoObj = userInfo;
        if (typeof (userInfo) == "string")
        {
            userInfoObj = JSON.parse(userInfo);
        }

        var getFullJson = [];
        getFullJson.push({"action": "getHourlyRate", "userId": userInfoObj[0].id});
        getFullJson = JSON.stringify(getFullJson);

        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + getFullJson,
            dataType: 'json',
            success: function (response)

            {

                $("#refresh_overlay").css('display', 'none');
                var finalHTML = '';

                if (response.code == 1003) {
                    $.each(response.data, function (index, result)
                    {

                        finalHTML += '<tr><td>' + (parseInt(index) + 1) + '</td><td>' + result.hourly_name + '</td><td><button class="btn btn-primary viewSpecial"  seq="' + result.id + '"> view</button></td><td><button class="btn btn-primary editHourly" seq="' + result.id + '" > Edit</button>&nbsp&nbsp<button class="btn btn-primary deleteHourly" seq="' + result.id + '"> Delete</button></td></tr>';

                    });

                } else {

                    finalHTML = '<tr><td colspan="1">Data not Found.</td></tr>';

                }
                $('#rate_set_up_list').html(finalHTML);
                $('.viewSpecial').on("click", function () {
                    $('#refresh_overlay').css("display", "block");
                    var getseq = $(this).attr("seq");
                    var fullJson = [];
                    fullJson.push({"action": "viewSpecialHourly",
                        "rowId": getseq
                    });
                    fullJson = JSON.stringify(fullJson);
                    rateSetupFinalClass.viewSpecialHourly(fullJson);

                });

                $('.editHourly').on("click", function () {

                    $('#refresh_overlay').css("display", "block");
                    var getseq = $(this).attr("seq");
                    var fullJson = [];
                    fullJson.push({"action": "viewSpecialHourly",
                        "rowId": getseq
                    });
                    fullJson = JSON.stringify(fullJson);
                    rateSetupFinalClass.editSpecialHourly(fullJson);
                });

                $('.deleteHourly').on("click", function () {

                    var getseq = $(this).attr("seq");
                    var getVarification = confirm("Are you Sure !");
                    var fullJson = [];
                    if (getVarification)
                    {
                        $('#refresh_overlay').css("display", "block");
                        fullJson.push({"action": "deleteHourlyRate",
                            "rowId": getseq
                        });
                        fullJson = JSON.stringify(fullJson);
                        rateSetupFinalClass.deleteHourlyRate(fullJson);
                    }

                });

            }
        });
    },

    /*---------------------------------------------
     Function Name: updateHourlyRate
     Input Parameter:JsonFullData 
     return:json data
     ---------------------------------------------*/
    updateHourlyRate: function (JsonFullData)
    {

        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + JsonFullData,
            dataType: 'json',
            success: function (response)
            {
                window.location.reload();
            }
        });
    },

    /*---------------------------------------------
     Function Name: setHourlyRate
     Input Parameter:JsonFullData 
     return:json data
     ---------------------------------------------*/
    setHourlyRate: function (JsonFullData)
    {
        
        //console.log(JsonFullData);
        $.ajax({
            url: rateSetupFinalClass._SERVICEPATH,
            type: 'POST',
            data: "FullResult=" + JsonFullData,
            dataType: 'json',
            success: function (response)
            {
                alert(response);
                $('#refresh_overlay').css("display", "none");
                console.log(response);
                if (response == null)
                {
                    alert("Hourly rate name already exist");
                } else
                {
                    alert("Hourly rate set successfully");
                    window.location.reload();
                }
            }
        });

    }
}


/*set the hourly rate setup on page load*/
rateSetupFinalClass.getHourlyRate();

/*---------------------------------------------
 Function Name: add the form dat on form submit
 Input Parameter:All form data 
 return:json data
 ---------------------------------------------*/
$('#rateSetUpAddForm').on("submit", function (e) {
    e.preventDefault();
    $('#refresh_overlay').css("display", "block");
    var finalJson = [];
    var getVehicleValue = [];
    var setRadius = [];
    var setBlackoutValue = [];
    if ($('.setRadius').is(":checked"))
    {
        
        var setRadiusValuemiles = $('.setRadiusValuemiles').val();
        
        var setRadiusValuehour = $('.setRadiusValuehour').val();
        setRadius.push(setRadiusValuemiles, setRadiusValuehour);
    } else
    {
        setRadius.push("notSet");

    }
    if ($('.setblacout').is(":checked"))
    {
        var setblacoutValuecuthour = $('.setblacoutValuecuthour').val();
        var setblacoutValueincreasehour = $('.setblacoutValueincreasehour').val();
        var setblacoutValueincreaserate = $('.setblacoutValueincreaserate').val();
        var setblacoutValueincreaserateType = $('.setblacoutValueincreaserateType').val();
        var eventSelectvalueCheck = $('#eventSelectvalue option:selected');
        var isCheckSelectValue = 0;
        $(eventSelectvalueCheck).each(function (index, result2) {
            
            isCheckSelectValue++;

        });
        if (isCheckSelectValue == 0)
        {
            alert("please select Black out date and special event");
            $('#refresh_overlay').hide()
            return 0;

        }
        setBlackoutValue.push(setblacoutValuecuthour, setblacoutValueincreasehour, setblacoutValueincreaserate, setblacoutValueincreaserateType);
    } else
    {
        setBlackoutValue.push("notSet")
    }
    $('.mnd_check').each(function () {

        var getValue = $(this).val();
        if ($(this).is(":checked"))
        {
            var getstdHour = $('.stdhrs' + getValue).val();
            var getstdPrice = $('.stdprice' + getValue).val();
            var vehicleCode = $('.dayName' + getValue).html().trim();
            var dschrs = $('.dschrs' + getValue).val();
            var dscprice = $('.dscprice' + getValue).val();
            var sdchrs = $('.sdchrs' + getValue).val();
            var sdcprice = $('.sdcprice' + getValue).val();

            getVehicleValue.push({
                "getstdHour": getstdHour,
                "getstdPrice": getstdPrice,
                "vehicleCode": vehicleCode,
                "dschrs": dschrs,
                "dscprice": dscprice,
                "sdchrs": sdchrs,
                "sdcprice": sdcprice
            });

        }

    });

    var eventSelectvalue = $('#eventSelectvalue option:selected');
    var hrlySmaValue = $('#hrlySmaValue option:selected');
    var eventArray = [];
    var hrlySmaValueArray = [];
    $(eventSelectvalue).each(function (index, result)
    {
        eventArray.push($(this).val());
    });
    if (!$('#checkSetDefault').is(":checked"))
    {
        $(hrlySmaValue).each(function (index, result)
        {
            hrlySmaValueArray.push($(this).val());
        });
    } else
    {
        hrlySmaValueArray.push("checkedTrue");

    }

    var userInfo = window.localStorage.getItem("companyInfo");
    var userInfoObj = userInfo;
    if (typeof (userInfo) == "string")
    {
        userInfoObj = JSON.parse(userInfo);
    }
    var hrly_rate_name = $('#hrly_rate_name').val();
    var gethtml = $('.hrlySetup').val();

    if (gethtml != 'update')
    {
        var selectBoxValue = $('#hrlySmaValue').val();
        if ($('#checkSetDefault').is(':checked') || selectBoxValue != null)
        {

            finalJson.push({"getVehicleValue": getVehicleValue,
                "setRadius": setRadius,
                "setBlackoutValue": setBlackoutValue,
                "getVehicleValue": getVehicleValue,
                "eventArray": eventArray,
                "hrlySmaValueArray": hrlySmaValueArray,
                "userId": userInfoObj[0].id,
                "hrly_rate_name": hrly_rate_name,
                "action": "setHourlyRate"

            });
            rateSetupFinalClass.setHourlyRate(JSON.stringify(finalJson));
        } else
        {
            alert("Must set as default or Select SMA DB");
            $('#refresh_overlay').css("display", "none");
        }

    } else
    {
        var getRowId = $('.hrlySetup').attr('seq');
        finalJson.push({"getVehicleValue": getVehicleValue,
            "setRadius": setRadius,
            "setBlackoutValue": setBlackoutValue,
            "getVehicleValue": getVehicleValue,
            "eventArray": eventArray,
            "hrlySmaValueArray": hrlySmaValueArray,
            "userId": userInfoObj[0].id,
            "hrly_rate_name": hrly_rate_name,
            "getRowId": getRowId,
            "action": "updateHourlyRate"

        });

        rateSetupFinalClass.updateHourlyRate(JSON.stringify(finalJson));

    }
});



/* page edit js end here */