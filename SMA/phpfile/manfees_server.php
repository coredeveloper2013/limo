<?php 

include_once 'config.php';
include_once 'comman.php';
define('WP_MEMORY_LIMIT', '564M');

/*****************************************************************
Method:             setManFees()
InputParameter:    	sma_id, vehicle_code, user_id
Return:             set Man Fees
*****************************************************************/
	function setManFees()
	{	
		if(isset($_REQUEST['sma_id'])&&(isset($_REQUEST['vehicle_code'])  )&&(isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
	   	{
      		$userId=$_REQUEST['user_id'];
		   	$VehicleCode=explode(',',$_REQUEST['vehicle_code']);
		   	$addSma=explode(',',$_REQUEST['sma_id']);
		   	$serviceTypeOptionBtnArray=explode(',',$_REQUEST['serviceTypeOptionBtnArray']);
			$isCheckValueExist="select * from mandatory_fees where toll='".$_REQUEST['amount_toll']."'";;
			$isCheckValueExistResult = operations($isCheckValueExist);
			if($isCheckValueExistResult!=1)
			{
				return global_message(200,1003);
			}
			
			$query ="insert into mandatory_fees(stc,fuel,admin,reserve,credit,black_car,toll,user_id,subtotal_tax) value('".$_REQUEST['amount_stc']."','".$_REQUEST['amount_fuel']."','".$_REQUEST['amount_admin']."','".$_REQUEST['amount_reserve']."','".$_REQUEST['amount_credit']."','".$_REQUEST['amount_black']."','".$_REQUEST['amount_toll']."','".$userId."','".$_REQUEST['sub_amount_tax']."')";  
			$resource = operations($query);
			$mf_id=mysql_insert_id();
			$srchargeValue=json_decode($_REQUEST['getSrchgValue']);
		
			for($i=0; $i<count($srchargeValue); $i++)
			{	
				$query2="insert into mandatory_fees_extra(parent_id,custom_fee_name,custom_fee,type_rate,is_subtamount_check) values('".$mf_id."','".$srchargeValue[$i]->feeName."','".$srchargeValue[$i]->feeRate."','".$srchargeValue[$i]->typeRate."','".$srchargeValue[$i]->checkGrandTotal."')";
				operations($query2);
			}

			for($i=0;$i<count($VehicleCode);$i++)
	  		{
		  		$Vehquery="insert into mf_vehicle(mf_id,vehicle_code,user_id) value('".$mf_id."','".$VehicleCode[$i]."','".$userId."')";	
		  		$resource1 = operations($Vehquery);
	  		}//VehicleCode
		  	for($j=0;$j<count($addSma);$j++)
	  		{
				$Smaquery="insert into mf_sma(mf_id,sma_id,user_id) value('".$mf_id."','".$addSma[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}

		  	for($j=0;$j<count($serviceTypeOptionBtnArray);$j++)
	  		{
				$Smaquery="insert into mandatory_service_type(parent_id,service_type,user_id) value('".$mf_id."','".$serviceTypeOptionBtnArray[$j]."','".$userId."')";	
		  		$resource2 = operations($Smaquery);
		 	}			
			$result=global_message(200,1008,$mf_id);		   
	   	}
   		else
	   	{
	    	$result=global_message(201,1003);
   		}	
		return $result;	
	}

/*****************************************************************
Method:             getRateMatrix List()
InputParameter:    	user_id
Return:             get Rate Matrix List
*****************************************************************/
	function getRateMatrixList()
	{
		if((isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id'])))
 	 	{
			$query="Select * from mandatory_fees where user_id='".$_REQUEST['user_id']."' order by toll asc";
			$resource= operations($query);
			$contents = array();
	    	if(count($resource)>0 && gettype($resource)!="boolean")
		 	{
				for($i=0; $i<count($resource); $i++)
				{
					$vehicle_code=''; 
					$sma_name='';
					$sma_id='';
					$value='';
					$Vehquery="Select vehicle_code from mf_vehicle where mf_id=".$resource[$i]['id'];
					$resource1= operations($Vehquery);
					for($j=0; $j<count($resource1); $j++)
						{
							$vehicle_code .=$resource1[$j]['vehicle_code'].',';
						}
					$driver_gratuity_query="Select service_type ,b.service_name from mandatory_service_type a inner join comman_service_type b on a.service_type=b.service_code  where a.parent_id=".$resource[$i]['id']." and b.user_id='".$_REQUEST['user_id']."'";

					$service_typeResult= operations($driver_gratuity_query);
					$service_typeArrayDublicate=[];
					$service_typeArray='';				
					$service_nameArrayDublicate=[];
					$service_nameArray='';
					for($m=0; $m<count($service_typeResult); $m++)
					{
						if(!in_array($service_typeResult[$m]['service_type'],$service_typeArrayDublicate))
						{
							array_push($service_typeArrayDublicate,$service_typeResult[$m]['service_type']);
							$service_typeArray .=$service_typeResult[$m]['service_type'].',';
							$service_nameArray .=$service_typeResult[$m]['service_name'].',';
						}
						
					}

				$Smaquery="Select sma_id,sma_name from mf_sma,sma where sma.id=mf_sma.sma_id AND mf_sma.mf_id=".$resource[$i]['id'];
				$resource2= operations($Smaquery);
				for($k=0; $k<count($resource2); $k++)
				{
					$sma_name .=$resource2[$k]['sma_name'].',';
					$sma_id .=$resource2[$k]['sma_id'].',';
				}

				$query12="select * from mandatory_fees_extra where parent_id=".$resource[$i]['id'];
				$resource123= operations($query12);
				$contents[$i]['id']=$resource[$i]['id'];
				$contents[$i]['stc']=$resource[$i]['stc'];
				$contents[$i]['fuel']=$resource[$i]['fuel'];
				$contents[$i]['admin']=$resource[$i]['admin'];
				$contents[$i]['reserve']=$resource[$i]['reserve'];
				$contents[$i]['credit']=$resource[$i]['credit'];
				$contents[$i]['subtotal_tax']=$resource[$i]['subtotal_tax'];
				$contents[$i]['black']=$resource[$i]['black_car'];
				$contents[$i]['toll']=$resource[$i]['toll'];
				$contents[$i]['sma_id'] = $sma_id;
				$contents[$i]['service_type'] = $service_typeArray;
				$contents[$i]['service_name'] = $service_nameArray;
				$contents[$i]['sma_name'] = $sma_name;
				$contents[$i]['vehicle_code']=$vehicle_code;
				$contents[$i]['srchrg']=$resource123;
			}
			}
			if(count($contents)>0 && gettype($contents)!="boolean")
		   	{
		   		$result=global_message(200,1007,$contents);
	   	   	}
		   	else
		   	{
	   			$result=global_message(200,1006);
		   	}		  
		}
 		else
  		{
	  		$result=global_message(201,1003);
  		}
  		return  $result;
	}


/*****************************************************************
Method:             deleteMandatoryFees()
InputParameter:    	user_id
Return:             delete Mandatory Fees
*****************************************************************/
	function deleteMandatoryFees()
	{
	 	if((isset($_REQUEST['mf_id']) && !empty($_REQUEST['mf_id'])))
	   	{
	  		$rowId=$_REQUEST['mf_id'];
			$query="delete from mandatory_fees where id='".$rowId."'";
		    $resource = operations($query);
		    $query12="delete from mandatory_fees_extra where parent_id='".$rowId."'";
		    $resource = operations($query12);
			$queryDelete1="delete  from mf_sma where mf_id='".$rowId."'";
			$resource2 = operations($queryDelete1);
			$queryDelete3="delete  from mandatory_service_type where parent_id='".$rowId."'";
			$resource3 = operations($queryDelete3);
			$queryDelete2="delete  from mf_vehicle where mf_id='".$rowId."'";
			$resource3 = operations($queryDelete2);
			$result=global_message(200,1010);   
		}
  		else
  		{
	   		$result=global_message(201,1003);
  		}
		return $result;
	}

