
/*---------------------------------------------
  Template Name: Mylimoproject
  Page Name: Edit Airport Page
  Author: Mylimoproject
---------------------------------------------*/ 

/*declare a class edit vehicle*/
var EditVehicle={
  /*set a web service path using php*/
	_Serverpath:"phpfile/airlines_client.php",
  
 


   



  /*---------------------------------------------
    Function Name: editAirportInformationShow
    Input Parameter:rowId
    return:json data
  ---------------------------------------------*/
	editAirportInformationShow:function(rowId){
     $('#refresh_overlay').css("display","block");
		 var getForm={"action":"editAirportInformationShow","rowId":rowId};
     $.ajax({
          url:EditVehicle._Serverpath,
          type:'POST',
          dataType:'json',
          data:getForm
     }).done(function(response){
           
           $('#country_name').val(response.data[0].airport_country);
           $('#state_name').val(response.data[0].state_name);
           $('#city_name').val(response.data[0].city_name);
           $('#city_zipCode').val(response.data[0].city_code);
           $('#airport_name').val(response.data[0].airport_code[0].airport_name);
           $('#airport_code').val(response.data[0].airport_code[0].airport_code);
      		 $('#selected_vehicle_rat_btn').attr("seq",response.data[0].id);
           $('#showAirportData').show();
        $('#refresh_overlay').css("display","none");   
    });
  
	},
  /*---------------------------------------------
    Function Name: deleteAirportInformation
    Input Parameter:rowId
    return:json data
  ---------------------------------------------*/
	deleteAirportInformation:function(rowId){
		 var getForm={"action":"deleteAirportInformation","rowId":rowId};
       $.ajax({
            url:EditVehicle._Serverpath,
            type:'POST',
            dataType:'json',
            data:getForm
       }).done(function(response){
       	
       	  EditVehicle.getAirportInformation();

      });

	},
 /*---------------------------------------------
    Function Name: deleteAirportInformation
    Input Parameter:rowId
    return:json data
  ---------------------------------------------*/
    getAirportInformation:function(rowId)
    {
      $('#refresh_overlay').css("display","block");
      var getUserId=window.localStorage.getItem('companyInfo');
          getUserId=JSON.parse(getUserId);
    	var getForm={"action":"getAirlinesInformation","rowId":rowId,"user_id":getUserId[0].id};
       $.ajax({
           url:EditVehicle._Serverpath,
           type:'POST',
           dataType:'json',
           data:getForm
       }).done(function(response){
       	
       if(response.code==1003)
        {
        	var getHTMl='';
        	console.log("response....",response);
      		for(var i=0; i<response.data.length; i++)
      		{
             
        		if(typeof(response.data[i].airport_code[0].airport_name)!=undefined)
            {
              getHTMl+='<tr><td>'+(i+1)+'</td><td>'+response.data[i].name+'</td><td>'+response.data[i].airline_code+'</td> <button class="btn btn-primary editClass" style="margin-right: 5%;" seq="'+response.data[i].id+'">Edit</button><button class="btn btn-primary deleteClass" seq="'+response.data[i].id+'">Delete</button></td></tr>';

            }

		      }

        }else{

           getHTMl+='<tr><td colspan="4">Data not Found.</td></tr>';
   
        }
        $('#view_airport_information_db_table').html(getHTMl);
        $('#refresh_overlay').css("display","none");
        $('.editClass').on("click",function(){
            var getSeq=$(this).attr("seq");
            EditVehicle.editAirportInformationShow(getSeq);

        });
        $('.deleteClass').on("click",function(){
          var getAns=confirm("Are you sure. you want to delete item");
          if(getAns)
          {
            var getSeq=$(this).attr("seq");
            EditVehicle.deleteAirportInformation(getSeq);

          }
          
        });

      });

    }
}
EditVehicle.getAirportInformation();

$('#showAddForm').on('click',function(){

  $('#airlinePopupStart').show();
})

$('#airlineform').on("submit",function(event){
  $('#refresh_overlay').css("display","block");
    event.preventDefault();

    var getUserId=window.localStorage.getItem('companyInfo');
          getUserId=JSON.parse(getUserId);


          
    var getNewForm=new FormData($("#airlineform")[0]);
    getNewForm.append("action","addAirlines");
    getNewForm.append("user_id",getUserId[0].id);
          $.ajax({
              url:EditVehicle._Serverpath,
                type:'POST',
                dataType:'json',
                data:getNewForm,
                contentType: false,
                processData: false,
                }).done(function(response) {
                    alert('Successfully Added');
                       EditVehicle.getAirportInformation();
                       $('#airportInformation')[0].reset(); 
                       $('#showAirportData').hide();
                  $('#refresh_overlay').css("display","none");

          }).fail(function(jqXHR, exception)
                {
 
                  $('#refresh_overlay').css("display","none");

          });
     });


$('#airportInformation').on("submit",function(event){
  $('#refresh_overlay').css("display","block");
    event.preventDefault();
    var getNewForm=new FormData($("#airportInformation")[0]);
    var getSeq=$('#selected_vehicle_rat_btn').attr("seq");
    	getNewForm.append("rowId",getSeq);
    	getNewForm.append("action","editClassAirport")
          $.ajax({
          	 	url:EditVehicle._Serverpath,
                type:'POST',
                dataType:'json',
                data:getNewForm,
                contentType: false,
                processData: false,
                }).done(function(response) {
                    alert('Successfully Updated');
                       EditVehicle.getAirportInformation();
                       $('#airportInformation')[0].reset(); 
                       $('#showAirportData').hide();
                  $('#refresh_overlay').css("display","none");

          }).fail(function(jqXHR, exception)
                {
 
                  $('#refresh_overlay').css("display","none");

          });
     });